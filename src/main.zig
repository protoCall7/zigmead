const std = @import("std");
const getopt = @import("getopt");

// nitrogen factors
const LOW = 0.75;
const MEDIUM = 1.0;
const HIGH = 1.25;

pub fn main() anyerror!void {
    var og: f64 = 0.0;
    var gallons: f64 = 0.0;
    var n_factor: f64 = 0.0;

    var opts = getopt.getopt("o:g:n:");
    while (opts.next()) |maybe_opt| {
        if (maybe_opt) |opt| {
            switch (opt.opt) {
                'o' => {
                    og = try std.fmt.parseFloat(f64, opt.arg.?);
                },
                'g' => {
                    gallons = try std.fmt.parseFloat(f64, opt.arg.?);
                },
                'n' => {
                    n_factor = switch (opt.arg.?[0]) {
                        'l' => LOW,
                        'm' => MEDIUM,
                        'h' => HIGH,
                        else => unreachable,
                    };
                },
                else => unreachable,
            }
        } else break;
    } else |err| {
        switch (err) {
            getopt.Error.InvalidOption => std.debug.print("invalid option: {c}\n", .{opts.optopt}),
            getopt.Error.MissingArgument => std.debug.print("option requires an argument: {c}\n", .{opts.optopt}),
        }
    }

    const brix = sgToBrix(og);
    const fermaid = calcFermaid(brix, n_factor, gallons);

    std.debug.print("{d}g fermaid-o\n", .{fermaid});
    std.debug.print("{d}lbs honey\n", .{calcHoneyRequired(og, gallons)});
}

// Fermaid formula: https://www.meadmaderight.com/nutrient-additions
fn calcFermaid(brix: f64, n_factor: f64, gallons: f64) f64 {
    return (((brix * 10) * n_factor) / 50) * gallons;
}

// SG to Brix formula: https://homebrewacademy.com/specific-gravity-to-brix/
fn sgToBrix(sg: f64) f64 {
    return (((182.4601 * sg - 775.6821) * sg + 1262.7794) * sg - 669.5622);
}

fn calcHoneyRequired(target_og: f64, gallons: f64) f64 {
    return ((target_og - 1) / 0.035) * gallons;
}
